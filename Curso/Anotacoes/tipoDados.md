# Tipo de dados

| C# Type | .Net Framework type | Signed | Bytes | Possible Values                                                                      |
|---------|--:-:----------------|--:-:---|--:-:--|--:-:---------------------------------------------------------------------------------|
| sbyte   | System.Sbyte        | Yes    | 1     | -128 to 127                                                                          |
| short   | System.Int16        | Yes    | 2     | -32768 to 32767                                                                      |
| int     | System.Int32        | Yes    | 4     | -2<sup>31</sup> to 2<sup>31</sup> -1                                                 |
| long    | System.Int64        | Yes    | 8     | -2<sup>63</sup> to 2<sup>63</sup> -1                                                 |
| byte    | System.Byte         | No     | 1     | 0 to 255                                                                             |
| ushort  | System.Uint16       | No     | 2     | 0 to 65535                                                                           |
| uint    | System.Uint32       | No     | 4     | 0 to 2<sup>32</sup> -1                                                               |
| ulong   | System.Uint64       | No     | 8     | 0 to 2<sup>64</sup> -1                                                               |
| float   | System.Single       | Yes    | 4     | �1.5 x 10<sup>-45</sup> to �3.4 x 10<sup>38</sup>  with 7 significant figures        |
| double  | System.Double       | Yes    | 8     | �5 x 10<sup>-324</sup> to �1.7 x 10<sup>308</sup>  with 15 or 16 significant figures |
| decimal | System.Decimal      | Yes    | 12    | �1 x 10<sup>-28</sup> to �7.9 x 10<sup>28</sup>  with 28 or 29 significant figures   |
| char    | System.Char         | N/A    | 2     | Any Unicode character                                                                |
| bool    | System.Boolean      | N/A    | 1/2   | true or false                                                                        |



## Restri��es para nomes de vari�veis

* N�o pode come�ar com d�gito: use uma letra ou _ 
* N�o usar acentos ou til
* N�o pode ter espa�o em branco
* Sugest�o: use nomes que tenham um significado

| Errado                      | Correto                   |
|-:-:-------------------------|-:-:-----------------------|
| int 5minutos;               | int _5minutos;            |
| int sal�rio;                | int salario;              |
| int sal�rio do funcionario; | int salarioDoFuncionario; |


## Conven�oes

* Camel Case: lastName (par�metros de m�todos, vari�veis dentro de m�todos).
* Pascal Case: LastName (Namespaces, classe, properties e m�todos).
* Padr�o Privado: _lastName (atributos "internos" da classe).