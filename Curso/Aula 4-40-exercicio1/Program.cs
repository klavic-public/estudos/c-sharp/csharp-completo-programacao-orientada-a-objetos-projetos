﻿using System.Globalization;
using Aula_4_40_exercicio1;

Pessoa pessoa1 = new Pessoa();
Pessoa pessoa2 = new Pessoa();

Console.WriteLine("Entre com os dados da primeira pessoa");
Console.WriteLine("Nome");
pessoa1.Nome = Console.ReadLine();
Console.WriteLine("Idade");
pessoa1.Idade = Int16.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

Console.WriteLine("");
Console.WriteLine("Entre com os dados da segunda pessoa");
Console.WriteLine("Nome");
pessoa2.Nome = Console.ReadLine();
Console.WriteLine("Idade");
pessoa2.Idade = Int16.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

string nomePessoaMaisVelha = pessoa1.Idade > pessoa2.Idade ? pessoa1.Nome : pessoa2.Nome;
Console.WriteLine($"Pessoa mais velha {nomePessoaMaisVelha}");
