﻿using System;

namespace Aula_4_41
{
    internal class Triangulo
    {
        public double A;
        public double B;
        public double C;

        public double Area()
        {
            double pX = (this.A + this.B + this.C) / 2.0;
            return Math.Sqrt(pX * (pX - this.A) * (pX - this.B) * (pX - this.C));
        }
    }
}
