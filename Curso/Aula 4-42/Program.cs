﻿using Aula_4_42;
using System.Globalization;

Produto p = new Produto();

Console.WriteLine("Entre os dados do produto:");

Console.Write("Nome: ");
p.Nome = Console.ReadLine();

Console.Write("Preço: ");
p.Preco = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

Console.Write("Quantidade no estoque: ");
p.Quantidade = int.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

Console.WriteLine("Dados do produto: " + p);

Console.WriteLine();
Console.Write("Digite o número de produtos a ser adicionado ao estoque:");
int qtdAdicionar = int.Parse(Console.ReadLine());
p.AdicionarProdutos(qtdAdicionar);
Console.WriteLine("Dados atualizados: " + p);

Console.WriteLine();
Console.Write("Digite o número de produtos a ser removidoao estoque:");
int qtdRemover = int.Parse(Console.ReadLine());
p.RemoverProdutos(qtdRemover);
Console.WriteLine("Dados atualizados: " + p);