﻿using System.Globalization;

namespace secao3
{
    internal class Program
    {
        static void Main(string[] args)
        {
          
        }
         
        static void DeclaracaoVariaveis()
        {
            sbyte x = 100;
            Console.WriteLine(x);

            byte n1 = 254;
            Console.WriteLine(n1);

            int n2 = 1000;
            Console.WriteLine(n2);

            int n3 = 2147483647;
            Console.WriteLine(n3);

            long n4 = 2147483648L;
            Console.WriteLine(n4);

            bool completo = false;
            Console.WriteLine(completo);

            char genero = 'F';
            Console.WriteLine(genero);

            char letra = '\u0041';
            Console.WriteLine(letra);

            float n5 = 4.5f;
            Console.WriteLine(n5);

            double n6 = 4.6;
            Console.WriteLine(n6);

            string nome = "Maria Green";
            Console.WriteLine(nome);

            object obj1 = "Alex Brown";
            Console.WriteLine(obj1);

            object obj2 = 4.5;
            Console.WriteLine(obj2);

            Console.WriteLine(int.MinValue);
            Console.WriteLine(int.MaxValue);
            Console.WriteLine(decimal.MaxValue);
        }

        static void SaidaDados()
        {
            Console.Write("Bom dia!");
            Console.WriteLine("Boa tarde!");
            Console.WriteLine("Boa note!");


            char genero = 'F';
            int idade = 32;
            double saldo = 10.35784;
            string nome = "Maria";
            
            Console.WriteLine("------------------------");
            Console.WriteLine();
            Console.WriteLine(genero);
            Console.WriteLine(idade);
            Console.WriteLine(saldo);
            Console.WriteLine(saldo.ToString("F2"));
            Console.WriteLine(saldo.ToString("F4"));
            Console.WriteLine(saldo.ToString("F4",CultureInfo.InvariantCulture));
            Console.WriteLine(nome);

            Console.WriteLine();
            Console.WriteLine("------------------------------");

            Console.WriteLine("Placeholder");
            Console.WriteLine("{0} tem {1} anos e tem saldo igual a {2:F2} reais.",nome,idade,saldo);
            Console.WriteLine("");

            Console.WriteLine("Interpolação");
            Console.WriteLine($"{nome} tem {idade} anos e tem saldo igual a {saldo:F2} reais.");
            Console.WriteLine();

            Console.WriteLine("Concatenação");
            Console.WriteLine(nome + " tem " + idade + "anos e tem saldo igual a "+ saldo.ToString("F2") + "reais.");
            Console.WriteLine();
        }

        static void SaidaDadosExercicio()
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Exercicio");
            
            string produto1 = "Computador";
            string produto2 = "Mesa de escritório";

            byte idade = 30;
            int codigo = 5290;
            char genero = 'M';

            double preco1 = 2100.0;
            double preco2 = 650.50;
            double medida = 53.234567;

            Console.WriteLine("Produtos:");
            Console.WriteLine($"{produto1}, cujo preço é $ {preco1:F2}");
            Console.WriteLine($"{produto2}, cujo preço é $ {preco2:F2}");
            Console.WriteLine("");
            Console.WriteLine($"Registro: {idade} anos de idade, código {codigo} e gênero: {genero}");
            Console.WriteLine("");
            Console.WriteLine($"Medida com oito casas decimais: {medida:F8}");
            Console.WriteLine($"Arredondamento (três casas decimais): {medida:F3}");
            Console.WriteLine($"Separador decimal invariant cultural {medida.ToString("F3",CultureInfo.InvariantCulture)}");

            Console.WriteLine("");
            Console.WriteLine("");
        }

        static void operacoesAtribuicao()
        {

        }
    }
}